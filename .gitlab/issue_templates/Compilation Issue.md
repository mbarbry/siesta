**Summary:**
<!--
Summarize the bug encountered concisely. Try to use also a
descriptive title for the issue.
-->


**Code version:**
<!--
Please indicate which Siesta version you used
(e.g., 4.1.5, or MaX-1.2.0-2-g598d19617).
This is vital since some bugs are already fixed in the development
branch.

This information can be found in file `SIESTA.version`
(for recent versions of Siesta) or file `version.info`
(for older versions of Siesta; in this case,
if you obtained the source code using git,
please specify the precise Commit Id hash).
-->


**System information:**
<!--
Please provide:
- which operating system and version
- which compiler and version used
- which libraries and versions used
- the arch.make file.
- clarify whether the bug happens in serial or parallel runs

Ideally you should check this using two different compilers to ensure this
is not compiler/platform bugs.
-->


**Compilation output:**
<!--
Is it when compiling Siesta or any of its utilities.

Either attach here, use gist's or pastebin.
-->

**Possible fixes**
<!--
If you have any idea how to fix this, please do not hesitate to help us out!
Code, links etc. are very welcome!
-->


/label ~Bug
